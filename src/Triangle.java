public class Triangle extends Shape implements TwoDimensionalShapeInterface {
	
	private double base;
	private double height;

	public Triangle(String color, int dimensions,double base,double height) {
		super(color, dimensions);
		
		this.base = base;
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	

}
