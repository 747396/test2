
public class Shape implements TwoDimensionalShapeInterface {
	
	private String color;
	private int dimensions;
	
	public Shape(String color,int dimensions) {
		this.color = color;
		this.dimensions = dimensions;
	}
	
	
	

	public String getColor() {
		return color;
	}




	public void setColor(String color) {
		this.color = color;
	}




	public int getDimensions() {
		return dimensions;
	}




	public void setDimensions(int dimensions) {
		this.dimensions = dimensions;
	}




	@Override
	public double calculateArea() {
		
		return 0;
	}

	@Override
	public void printInfo() {
		System.out.println("The color of the shape is :" + color);
		System.out.println("The dimensions of the shape is :"+ dimensions);
		
		
	}
	

}
