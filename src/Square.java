
public class Square extends Shape implements TwoDimensionalShapeInterface {
	private int sides;

	public Square(String color, int dimensions,int sides) {
		super(color, dimensions);
		this.sides = sides;
		
	}

	public int getSides() {
		return sides;
	}

	public void setSides(int sides) {
		this.sides = sides;
	}

}
